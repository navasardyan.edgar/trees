class TreePrinter():
    def __init__(self, tree, type = 'diagram'):
        self.tree =  tree
        self.type = type

    def __str__(self, *args, **kwargs):
        print('Hello, TreePrinter')

class BinaryTree:

    def __init__(self, data):
        self.left = None # BinaryTree
        self.right = None # BinaryTree
        self.data = data


    def __str__(self):
        self.print()

    def print(self):
        print()


    def insert(self, data):
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = BinaryTree(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = BinaryTree(data)
                else:
                    self.right.insert(data)

        else:
            self.data = data

    def get_node_level(self, node):
        if self.data == 0:


if __name__ == "__main__":
    x = BinaryTree(1)
    x.insert(2)
    x.insert(5)
    TreePrinter(x)